---
title: "Vergleich von Random Forest und Support Vector Machine"
author: "Sören Michaels"
date: "28.10.2020"
output: 
  pdf_document:
    toc: true
  html_document: 
    toc: yes
    keep_md: yes
bibliography: bib.bib
editor_options: 
  chunk_output_type: console
---



\newpage

# Einleitung

Ob, bei Abgrenzung einzelner Baumkronen [@liu2015novel], Dürrevorhersage und Bewertung sowie Kartierung von Vegetation [@szantoi2013analyzing], um eine computergestützte Verarbeitung und Klassifizierung mit Support Vector Machine und Random Forest kommt man fast nicht vorbei. Random Forest und Support Vector Machine sind sehr beliebte Algorithmen zur Klassifikation. In dieser Arbeit werden diese zwei beliebte Algorithmen miteinander verglichen.
Es wird eine kurze Beschreibung der Funktionsweise geben, in der die Unterschiede der beiden Algorithmen diskutiert werden.
Darauf folgt ein praktischer Teil, bei dem beide Algorithmen auf den gleichen Datensatz angewendet werden.
Die Ergebnisse werden validiert und ausgewertet.
Zum Schluss gibt es eine Fehleranalyse der methodischen Durchführung und ein Fazit.

\newpage

# Funktionsweise Random Forest

Random Forest ist ein Algorithmus der viele verschiedene randomisierte Entscheidungsbäume erstellt.
Jeder Entscheidungsbaum trifft seine Entscheidung.
Da die Bäume auf unterschiedliche Ergebnisse kommen entscheidet die Mehrheit der Bäume, wie die Daten klassifiziert werden[@livingston2005implementation]. 

Jeder Baum wird an den Knoten mit zufällig ausgewählten Eigenschaften oder einer Kombination aus verschieden Eigenschaften gespalten.

Es wird jeder Baum nicht mit dem Originaldatensatz erstellt, sondern mit einem sogenannten Bootstrapped Datensatz.
Der Prozess um einen Bootstrapped Datensatz zu erstellen heißt Bagging.

Bagging (kurz für Bootstrap aggregating) funktioniert, wie folgt: Für den Orginaldatensatz $D$ mit der Größe $n$, werden $m$ (Anzahl der Entscheidungsbäume) Trainingsdatensätze  $D_i$ erstellt, jeder mit der Größe $n'$.
Um die Traningsdatensätze $D_i$ zu erstellen, wird jeweils eine Zufallsstichprobe mit Zurücklegen erstellt[@friedman2001elements]. 

Ziel des Baggings ist es Overfitting zu vermeiden, dass Modell soll also nicht zu angepasst auf die Originaldaten sein.

Es handelt sich beim Random Forest um sogenanntes Ensamble learning, es werden mehre Alghorithmen miteinander benutzt, um eine bessere Perfomance zu erreichen.
Es werden mehrere schwache Klassifikatoren in diesem Fall Entscheidungsbäume miteinander "verschmolzen", um einen gut Klassifikator zu erstellen, dies wird auch Boosting genannt.
Ziel ist es aus den schwachen Klassifikatoren, die Fehler behaftet sind, in unserem Fall die Entscheidungsbäume, einen Klassifikator zu erstellen, der wenige Fehler macht.
Im Random Forest ist die Mehrheitswahl der Entscheidungsbäume die Verschmelzung der schwachen Klassifikatoren[@xu2019bearing]. 

Der Algorithmus geht wie folgt in Pseudocode:

1. für die Anzahl der Bäume
  1. bootstrapped_datensatz = ziehe mit zurücklegen aus dem Trainingsdatensatz mit der Größe $N$ einen Datensatz mit der Größe $N$
  2. erstelle einen Entscheidungsbaum im Random Forest aus dem boostrapped_datensatz bis die Mindestanzahl an Knoten erreicht ist
        1. von den Variablen werden $m$ ausgewählt
            2. verzweigungs_punkt = bester Punkt zum Teilen für Variablen $m$ wird ausgewählt
                3. mit dem verzweigungs_punkt werden zwei weitere Knotenpunkte erstellt
2. gebe Menge an Bäumen aus

3. Klassifikation mit nicht Trainingsdaten
  1. Liste mit Abstimmungen
  2. für jeden Baum
        1. füge neuen datensatz ein
            2. Baum trifft Entscheidung für Klasse
                3. Entscheidung wird in Liste mit Abstimmung gespeichert
  3. gebe Klasse mit den meisten Stimmen aus

Darstellung vergleichbar mit @friedman2001elements
    
\newpage

# Funktionsweise Support Vector Machine
![Hyperebene die mit maximalem Abstand zwei Klassen teilt [@SVMmargi82:online]](SVM_margin.png)

Der Support Vector Machine Algorithmus könnte genauer sein, als andere beliebte Techniken[@foody].
Das Besondere ist, dass SVM nur binär Klassen teilen kann.
Es gibt aber Methoden, um mit SVM auch eine mehrklassige Klassifikation durchzuführen.
Die Support Vector Machine teilt bei der Klassifikation die Klassengrenzen so ein, dass eine möglichst große Margin entsteht.
Der Bereich der frei von Objekten ist, soll also möglichst groß sein.
Dadurch, dass SVM immer versucht zwischen den Klassengrenzen den größten Abstand zu erzeugen, werden Punkte, die eigentlich in anderen Algorithmen nur sehr unsicher klassifiziert werden deutlicher voneinander getrennt [@manning2008introduction].
Um die Trennung mit dem größten Abstand zu erhalten werden die Punkte zwischen den beiden Klassen betracht, diese Punkte nennt man Stützvektoren, auf englisch support vectors.
Punkte die sich hinter den Stützvektoren befinden sind die sogenannten Trainingsvektoren, diese sind für die Klassifizierung mit SVM nicht von Bedeutung.
Sie liegen hinter den Stützvektoren und beeinflussen die Trennungsebene nicht.
Ist die Trennungsebene durch SVM gefunden dient die Funktion der Trennungsebene als Entscheidungsfunktion.
Nicht Trainingsdaten können nun mit dieser Entscheidungsfunktion klassifiziert werden.
Je nach dem auf welcher Seite der Entscheidungsfunktion ein neues Objekt ist, so wird es entsprechend klassifiziert.
Am einfachsten ist diese Trennung, wenn die Objekte linear teilbar sind.
Dabei werden die Klassengrenzen mit einer linearen Hyperebene gezogen [^1] [@adam].
Da man nicht immer die Klassen linear trennen kann kommt der sogenannte Kernel-Trick zum Einsatz.
Unter Kernel-Trick versteht man verschiedene Funktionen, um die Daten zu Transformieren.
Dieser funktioniert in dem die Trainingsdaten in höhere Dimensionen transformiert werden bis diese linear trennbar sind.
Da die Anzahl der möglichen Dimensionen unendlich ist, kann immer eine Möglichkeit gefunden werden die Klassengrenzen zuteilen. 
Um aber die Genauigkeit zu erhöhen, die Rechenleistung zu verringern und Overfitting zu vermeiden, kann mit einer Soft Margin gearbeitet werden.
Es wird also erlaubt, dass einige Objekte innerhalb der Marigin sich befinden falsch klassifiziert werden.
Dies führt dazu, das Ausreißer weniger gewichtet werden und auf der anderen Seite dazu, dass eine Hyperebene schneller gefunden werden kann.
Um mehr als zwei Klassen zu klassifizieren wird in der bekannten SVM Implementation libsvm, die auch in der e1071 Library in R benutzt wird, die one-versus-one Strategie benutzt. 
Es treten jeweils zwei Klassen gegeneinander an, die Klasse die Gewinnt erhält einen Punkt. 
Sind alle Klassen gegeneinander angetreten, gewinnt die mit den meisten Punkten [@manning2008introduction].

[^1]: Hyperebene = mathematischer Begriff für Ebenen mit n-Dimensionen, also im eindimensionalen Raum ist eine Hypereben ein Punkt im zweidimensionalen Raum eine Gerade usw.




# Random Forest und Support Vector Machine in der Praxis

Im Folgenden Teil soll der Bericht ein Beispiel einer Durchführung von SVM und RF in R geben. Es geht um die Auswahl der zu klassfizierenden Daten, Erstellen von Trainingsdaten, benutzen von R Paketen zur Anwendung von RF und SVM mit dem Trainieren von den Modellen und der anschließenden Klassifikation sowie die Durchführung der externen Validierung. Danach wird die Genauigkeit der erstellten Daten diskutiert mit möglichen Ursachen sowie Probleme, die bei der Durchführung entstanden sind. 

## Bildinformationen: Mit welchen Daten arbeiten wir

In dem Bericht wird mit simulierten Hyperspektraldaten gearbeitet.
Diese Daten wurden für das Environmental Mapping and Analysis Program kurz EnMAP erstellt.
EnMAP ist eine Deutsche Hyperspektral-Satelliten Mission, welche das Ziel hat wichtige dynamische Prozesse der Ökosysteme der Erde messen und zu modellieren. 
Im Rahmen der Vorbereitungsphase von EnMAP werden Flugzeuggestützt EnMAP Daten simuliert, welche frei verfügbar unter https://www.enmap.org/data_tools/flights/ sind [@okujeni2016berlin].

Diese Daten passen perfekt in das Thema des Moduls (Urbane Fernerkundung Berlin) in Laufe dieser Bericht erstellt wurde. 




## Aufteilen des Satellitenbild

Als Erstes wird das Satellitenbild in zwei Bilder geteilt.
Das eine Bild wird dazu genutzt die Trainingsdaten für SVM und RF zu erstellen und das andere Bild wird dazu benutzt die Klassifikatoren anzuwenden sowie um die Klassifikationen extern zu validieren.
Das Aufteilen wurde in QGIS durchgeführt in dem das Bild zwischen West und Ost in der Mitte geteilt wurde.



## Erstellung der Trainingsdaten

Auch die Trainingsdaten wurden in QGIS erstellt in dem Polygone für verschiedene Klassen erstellt wurden.

## Auswahl der Klassen
Die Auswahl der Klassen war ein längerer Prozess.
Es wurden immer wieder Klassen hinzugefügt und validiert bis eine passende Klasseneinteilung gefunden wurde.
Diese Klasseneinteilung war trotzdem nicht optimal, dies wird in der Auswertung weiter erläutert.

## Trainingsdaten in R laden und Pixel mit Klassen Zuordnung extrahieren

Um die Trainingsdaten in R nutzten zu können und somit RF und SVM trainieren zu können, brauchen wir zuerst das Trainingsbild, mit dem der Trainingsshapefile erstellt wurde und den Trainingsshapefile. Im Trainingsshapefile sind die Informationen, wo sich räumlich welche Klassen befinden im Bild. Die Pixel aus dem Trainingsbild stellen mit den Grauwerten die Reflexionseigenschaften für die Spektralkanäle dar. Sind beide Dateien in R geladen kann nun mithilfe des Trainingsshapefile ein Dataframe erstellt werden, welches die Pixel entsprechend der Klassen aus dem Bild extrahiert. Wir haben nun einen Datenframe mit einer Reihe pro Pixel und eine Spalte pro Spektralkanal plus einer Spalte zur Klassenzuordnung.

## Trainieren von RF und SVM

Mit unserem zuvor erstellten Dataframe können wir nun unsere Modelle trainieren. Für Random Forest benutzen wir das Paket randomForest und für Support Vector Machine nehmen wir das Paket e1071 mit der Funktion svm(). Zum Trainieren brauchen beide Pakte als ersten Parameter die Pixel mit den Spektraldaten und als zweiten Parameter die zugehörigen Klassen. 

## Anwenden von RF und SVM

Mit den beiden erstellten Modellen kann nun eine Klassifikation erstellt werden. In R benutzt man die Funktion predict(). Als erstes Argument braucht predict das Bild, auf dem die Modelle angewendet werden und als zweiten Parameter nimmt predict das Modell, welches zu vortrainiert wurde. 

## Darstellung von den Klassifikationen

Entweder stellt man die Ergebnisse über die Plotfunktion in R dar oder in einem anderen GIS Programm.

![RF vs SVM](SVM%20vs.%20RF_NEU.png)

Der erste Anschein lässt beide Klassifikationen sehr ähnlich erscheinen. Doch bei genauerer Betrachtung sieht es so aus, dass bei der RF-Klassifikation im Vergleich zur SVM-Klassifikation sehr viel mehr Pixel als rote Dächer und weniger Pixel als Schatten klassifiziert werden, dies muss mit den Unterschieden der Klassifikatoren zu tun haben, da die Trainingsdaten gleich sind. Rein visuell betrachtet scheint die Suppport-Vector-Machine-Klassifikation genauer am Originalbild zu sein. Ob dieser Schein trügt, wird sich in der Validierung zeigen. 


##  Validierung

RF hat eine interne Validierung diese lässt sich über den Namen der Validierung plus $confusion darstellen. In dieser internen Validierung werden von der die Objekte zum Validieren benutzt, die nicht im bootstrapped Datensatz gelangt sind. Es handelt sich hier bei um den sogenannten Out-Of-Bag-Error. Also die Klassifizierung wird anhand der Daten validiert, die nicht gezogen wurden. Doch der reale Fehler des RF ist meist höher als der OOB-Error. SVM besitzt keine interne Validierung. Um nun beide Klassifikatoren zu vergleichen, benötigt es eine externe Validierung. Bei einer externen Validierung wird die Klassifikation mit Daten validiert, welche nicht zum Klassifizieren, also in unserem Fall nicht zum trainieren von SVM und RF benutzt wurden.  Um die Klassifikation zu validieren werden pro Klasse eine bestimmte Anzahl von Punkten zufällig generiert, welche dann manuell referenziert werden. Diese Art der Stichprobenerstellung nennt sich stratifizierte Zufallsstichprobe [@fu]. In dieser Arbeit wurden für beide Klassifikation 20 Punkte pro Klasse referenziert. Anschließend wurden beide Referenzierungen verschmolzen, später mit ungefähr 40 Punkten pro Klasse zu validieren, dies hat den Vorteil, dass die Referenzierung nicht besser auf eine der beiden Klassifikationen angepasst ist. Es ist außerdem möglich die gleichen Referenzierungsdaten auf beide Klassifikation anzuwenden. Ist jeder Punkt referenziert kann nun mithilfe einer Konfusionsmatrix die verschieden Genauigkeiten berechnet werden. Es gibt die Benutzergenauigkeit, also die Genauigkeit die der Benutzer bei Validieren hat, aus Sicht der Klassifikation. Die Produktgenauigkeit ist die Genauigkeit, die die Klassifikation aus der Sicht des Nutzers hat, außerdem gibt es die Gesamtgenauigkeit. Die Gesamtengauigkeit sind die Klassen die beim Nutzer und bei der Klassifikation übereinstimmen geteilt durch die Klassenzuordnungen die nicht übereinstimmen. Anhand dieser Daten kann man evaluieren, wie gut die Verwendbarkeit der Klassifikation ist. Des Weiteren lässt sich mit der Konfusionsmatrix auch die Genauigkeit der einzelnen Klassen betrachten. Ist eine Klasse besonders oft falsch klassifiziert, könnten die Trainingsdaten überarbeitet werden oder als Hilfe andere Daten dazu genommen werden. Dies wird in der Auswertung weiter ausgeführt. 

### Interne Valdierung Random Forest


|                       |blaues Dach|graues Dach|offener Boden|roter Sportplatz|rotes Dach|Schatten|Schwimmbad mit blauen Fliesen|Vegetation|Versiegelung|Wasser|weißes Dach|class.error       |
|-----------------------------|-----------|-----------|-------------|----------------|----------|--------|-----------------------------|----------|------------|------|-----------|------------------|
|blaues Dach                  |0          |0          |0            |0               |0         |0       |0                            |1         |1           |0     |0          |1                 |
|graues Dach                  |0          |12         |0            |0               |0         |0       |0                            |0         |6           |0     |0          |0,333333333333333 |
|offener Boden                |0          |0          |54           |0               |1         |0       |0                            |0         |0           |0     |4          |0,0847457627118644|
|roter Sportplatz             |0          |0          |0            |65              |2         |0       |0                            |0         |0           |0     |0          |0,0298507462686567|
|rotes Dach                   |0          |0          |0            |2               |31        |1       |0                            |0         |1           |0     |0          |0,114285714285714 |
|Schatten                     |0          |0          |0            |0               |0         |20      |0                            |1         |0           |0     |0          |0,0476190476190477|
|Schwimmbad mit blauen Fliesen|0          |0          |0            |0               |0         |0       |0                            |0         |0           |0     |1          |1                 |
|Vegetation                   |0          |0          |0            |0               |0         |1       |0                            |61        |0           |0     |0          |0,0161290322580645|
|Versiegelung                 |0          |4          |0            |0               |1         |0       |0                            |0         |17          |0     |0          |0,227272727272727 |
|Wasser                       |0          |0          |0            |0               |0         |0       |0                            |0         |0           |136   |0          |0                 |
|weißes Dach                  |0          |0          |6            |0               |0         |0       |0                            |0         |0           |0     |40         |0,130434782608696 |



\newpage

# Auswertung

## SVM

|            |                               | References  |                   |               |                  |                   |                   |                               |                   |                   |                   |                   |                        |                   |                   |
| ---------- | ----------------------------- | ----------- | ----------------- | ------------- | ---------------- | ----------------- | ----------------- | ----------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ---------------------- | ----------------- | ----------------- |
| **Prediction** |                               | blaues Dach | graues Dach       | offener Boden | roter Sportplatz | rotes Dach        | Schatten          | Schwimmbad mit blauen Fliesen | Vegetation        | Versiegelung      | Wasser            | weißes Dach       | sums                   | same class        | producer accurarcy     |
|            | blaues Dach                   | 0           | 0                 | 0             | 0                | 0                 | 0                 | 0                             | 0                 | 0                 | 0                 | 0                 | 0                      | 0                 | -                 |
|            | graues Dach                   | 0           | 16                | 0             | 0                | 1                 | 0                 | 0                             | 0                 | 5                 | 0                 | 2                 | 24                     | 16                | 0.666666666666667 |
|            | offener Boden                 | 0           | 4                 | 15            | 0                | 0                 | 0                 | 0                             | 0                 | 4                 | 0                 | 6                 | 29                     | 15                | 0.517241379310345 |
|            | roter Sportplatz              | 0           | 0                 | 1             | 12               | 3                 | 0                 | 0                             | 1                 | 1                 | 0                 | 0                 | 18                     | 12                | 0.666666666666667 |
|            | rotes Dach                    | 0           | 7                 | 3             | 4                | 10                | 0                 | 0                             | 2                 | 7                 | 0                 | 0                 | 33                     | 10                | 0.303030303030303 |
|            | Schatten                      | 0           | 1                 | 0             | 0                | 0                 | 6                 | 0                             | 20                | 3                 | 0                 | 0                 | 30                     | 6                 | 0.2               |
|            | Schwimmbad mit blauen Fliesen | 0           | 0                 | 0             | 0                | 0                 | 0                 | 0                             | 0                 | 0                 | 0                 | 0                 | 0                      | 0                 | -                 |
|            | Vegetation                    | 0           | 2                 | 1             | 0                | 1                 | 0                 | 0                             | 40                | 1                 | 1                 | 0                 | 46                     | 40                | 0.869565217391304 |
|            | Versiegelung                  | 0           | 35                | 0             | 0                | 4                 | 1                 | 1                             | 0                 | 18                | 0                 | 2                 | 61                     | 18                | 0.295081967213115 |
|            | Wasser                        | 0           | 2                 | 0             | 0                | 0                 | 15                | 0                             | 0                 | 6                 | 14                | 0                 | 37                     | 14                | 0.378378378378378 |
|            | weißes Dach                   | 0           | 10                | 4             | 0                | 0                 | 0                 | 0                             | 0                 | 6                 | 0                 | 9                 | 29                     | 9                 | 0.310344827586207 |
|            | **sums**                      | 0           | 77                | 24            | 16               | 19                | 22                | 1                             | 63                | 51                | 15                | 19                |                        |                   |                   |
|            | **same class**                | 0           | 16                | 0             | 12               | 10                | 6                 | 0                             | 40                | 18                | 14                | 9                 | **overall same class** | 140               |                   |
|            | **user accuracy**             | -           | 0.207792207792208 | 0             | 0.75             | 0.526315789473684 | 0.272727272727273 | 0                             | 0.634920634920635 | 0.352941176470588 | 0.933333333333333 | 0.473684210526316 | **overall sum**        | 307               |                   |
|            |                               |             |                   |               |                  |                   |                   |                               |                   |                   |                   |                   | **overall accuaracy**  | 0.456026058631922 |                   |

## RF

|                |                               | References  |                   |               |                  |                  |                   |                               |                   |                   |                   |                   |                        |                   |                   |
| -------------- | ----------------------------- | ----------- | ----------------- | ------------- | ---------------- | ---------------- | ----------------- | ----------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ---------------------- | ----------------- | ----------------- |
| **Prediction** |                               | blaues Dach | graues Dach       | offener Boden | roter Sportplatz | rotes Dach       | Schatten          | Schwimmbad mit blauen Fliesen | Vegetation        | Versiegelung      | Wasser            | weißes Dach       | sums                   | same class        | producer accurarcy     |
|                | blaues Dach                   | 0           | 6                 | 0             | 0                | 0                | 0                 | 1                             | 5                 | 5                 | 0                 | 0                 | 17                     | 0                 | 0                 |
|                | graues Dach                   | 0           | 18                | 0             | 0                | 1                | 0                 | 0                             | 0                 | 4                 | 0                 | 0                 | 23                     | 18                | 0.782608695652174 |
|                | offener Boden                 | 0           | 5                 | 13            | 0                | 0                | 0                 | 0                             | 0                 | 5                 | 0                 | 5                 | 28                     | 13                | 0.464285714285714 |
|                | roter Sportplatz              | 0           | 1                 | 0             | 16               | 4                | 0                 | 0                             | 1                 | 2                 | 0                 | 0                 | 24                     | 16                | 0.666666666666667 |
|                | rotes Dach                    | 0           | 11                | 7             | 0                | 13               | 1                 | 0                             | 2                 | 11                | 0                 | 0                 | 45                     | 13                | 0.288888888888889 |
|                | Schatten                      | 0           | 3                 | 0             | 0                | 0                | 12                | 0                             | 20                | 4                 | 0                 | 0                 | 39                     | 12                | 0.307692307692308 |
|                | Schwimmbad mit blauen Fliesen | 0           | 0                 | 0             | 0                | 0                | 0                 | 0                             | 0                 | 0                 | 0                 | 0                 | 0                      | 0                 | -                 |
|                | Vegetation                    | 0           | 0                 | 0             | 0                | 0                | 0                 | 0                             | 35                | 0                 | 1                 | 0                 | 36                     | 35                | 0.972222222222222 |
|                | Versiegelung                  | 0           | 22                | 0             | 0                | 1                | 0                 | 0                             | 0                 | 9                 | 0                 | 0                 | 32                     | 9                 | 0.28125           |
|                | Wasser                        | 0           | 1                 | 0             | 0                | 0                | 9                 | 0                             | 0                 | 6                 | 14                | 0                 | 30                     | 14                | 0.466666666666667 |
|                | weißes Dach                   | 0           | 10                | 4             | 0                | 0                | 0                 | 0                             | 0                 | 5                 | 0                 | 14                | 33                     | 14                | 0.424242424242424 |
|                | **sums**                      | 0           | 77                | 24            | 16               | 19               | 22                | 1                             | 63                | 51                | 15                | 19                |                        |                   |                   |
|                | **same class**                | 0           | 18                | 0             | 16               | 13               | 12                | 0                             | 35                | 9                 | 14                | 14                | **overall same class** | 144               |                   |
|                | **user accuracy**             | -           | 0.233766233766234 | 0             | 1                | 0.68421052631579 | 0.545454545454545 | 0                             | 0.555555555555556 | 0.176470588235294 | 0.933333333333333 | 0.736842105263158 | **overall sum**        | 307               |                   |
|                |                               |             |                   |               |                  |                  |                   |                               |                   |                   |                   |                   | **overall accuaracy**  | 0.469055374592834 |                   |



Trotz der großen sichtbaren Unterschiede der beiden Klassifikationen wirken die Unterschiede in der Validierung nicht besonders groß. Der Unterschied liegt bei der Gesamtgenauigkeit nur bei 1,3 % zwischen RF und SVM zugunsten. RF war aus Sicht des Nutzers genauer bei den Klassen "graues Dach" mit 78.2 % vs. 66.7 %, bei den Schatten mit 30,8 % vs. 20 %, bei der Vegetation mit 97,2 % vs. 87.0 % und beim Wasser mit 46.7 % vs. 37.8 % sowie beim weißen Dächern mit 42.4 % vs. 31.0 %. SVM ist hingegen besser beim offenen Boden mit 51.7 % gegen 46.4 %, bei den roten Dächern mit 30.3 % vs. 28,9 % und bei der Versigelung mit 29.5 % vs. 28.1 %. Aus Nutzersicht ist Random Forest bei mehr Klassen genauer und das auch meistens mit höherer Differenz. Doch diese Aussage lässt sich nicht verallgemeinern. Nur wenn sich dieses Muster bei vielen verschiedenen Vergleichen zwischen RF und SVM wiederholt kann man Schlussfolgerungen ziehen, welcher der beiden Algorithmen besser geeignet ist. Je nach Trainingsdaten gibt es wahrscheinlich einen Unterschied in der Performance von SVM und RF. In diesem Fall lässt sich Sagen, dass der RF hier besser abgeschlossen hat, da sowohl die Nutzergenauigkeiten, als auch die Gesamtgenauigkeit besser sind als bei SVM.


Bei der Durchführung sind verschiedenste Probleme aufgetreten im folgenden Text, werden diese besprochen und mögliche Lösungen oder Lösungsansätze angesprochen.

Ein Problem ist, dass das Satellitenbild Schatten beinhaltete. Schatten, welche durch Häuser und sonstige Gebäude entstanden sind, waren leichter zu erkennen auf dem Satellitenbild und so mit konnten auch gut Trainingsdaten für diese erstellt werden. Doch im Laufe der Validierung zeigten sich Probleme bezüglich der Schatten auf. Als Referenz wurden bei der Validierung Daten von Google Earth aus dem Aufnahmejahr zur Hilfe genommen, doch da diese nicht zur gleichen Tages und Jahreszeit aufgenommen wurden und auch aus anderen Aufnahmebedingungen stammen, stimmen Schatten nicht mit dem Satelliten überein. Besonders Schatten die durch Wolken und Vegetation sind sehr unterschiedlich. Bei dieser Arbeit wurden deshalb nur Schatten extern validiert, die als solche eindeutig erkennbar sind, andere Schatten wurden ausgelassen, dies hat aber den negativen Effekt, dass für die Schatten weniger Punkte validiert wurden. Mehr und bessere Referenzdaten sind hier von Vorteil.   

Ein Hindernis war es auch, dass es Klassen, die es auf dem Trainingsbild gab, aber  nicht auf dem Testbild vorkamen, so gab es zum Beispiel auf dem Testbild Gewächshäuser mit Glasdächern, die auf dem Trainingsbild nicht vorkamen. Diese wurde im Bericht bei der externen Validierung nicht als falsch validiert, sondern ausgelassen, da es sich ja nicht um eine Falschklassifizierung durch den Algorithmus handelt, sondern um einen Fehler in den Trainingsdaten. Um diesen Fehler zu vermeiden, sollte die Klassen in Trainingsdaten und Testdaten oder in den Daten, auf die der Klassifikator angewendet werden soll übereinstimmen. Schließlich kann weder SVM noch RF in Klassen klassifizieren, die sie nicht kennen. 

Ein weitere Fehlerquelle ist die externe Validierung. Sie ist enorm zeitaufwendig und trotzdem kann hier im Bericht keine genaue Aussage daraus getroffen werden. Es wurden die zur
Verfügung stehenden Daten genutzt, um genau wie möglich die Wahrheit über den Boden zu erlangen. Es wurden zu dem simulierten Satellitenbild noch andere Karten von Diensten, wie Google Earth hinzugenommen. Doch um eine nah zu objektive Bewertung zu erlangen, bräuchte es Daten, die direkt am Aufnahme Tag parallel zur Aufnahme aufgenommen wurden. Also einer Überprüfung am Boden und vor Ort. Die externe Validierung im Laufe dieser Forschungsarbeit konnte aufgrund fehlender Daten oft nicht mit besonders hoher Sicherheit gemacht werden, welches ein reiner Vergleich von RF und SVM verlangen würde. 

Außerdem war ein Problem während der Durchführung, dass sich die Klassen oft sehr geähnelt haben, hier könnte man die spektralen Eigenschaften vergleichen und gegebenfalls die Klassen zusammenführen, dies hätte aber den Effekt, dass Klassen, die für unser menschliches Auge gut zu unterscheiden sind zusammen gelegt werden, wie z. B. graue Dächer und Versiegelung. Will man diese Klassen dennoch unterscheiden muss man auf andere Systeme der Klassifizierung zurückgreifen oder mit einer Kombination von verschiedenen Daten arbeiten. Dächer könnte man zum Beispiel mit einem 3D-Modell erkennen. Eine weitere Möglichkeit wäre es Objekt basiert die Dächer zu erkenn mit Software, wie eCognition. Während bei pixelbasierter Klassifikation die Pixel von Pixel zu Pixel basiert werden, werden beider Objekt basierten Klassifikation mehrere Pixelgruppen zusammengefasst. Es werden also Muster und Formen erkannt. Dadurch lassen sich die Dächer besser von der Versiegelung trennen, da Dächer oft markante Formen aufweisen, wie z. B. eckig mit Rand und Schornstein [@gupta2014object]. 


# Fazit

Abschließend ist festzustellen, dass SVM und RF sehr ähnlich im Vergleich abgeschlossen haben, obwohl die Funktionsweise komplett verschieden ist.
In diesem Bericht hat der RF leicht besser abgeschlossen, ob dies auch mit anderen Trainingsdaten so ist, kann nur gesagt werden in dem man ein Experiment im größeren Umfang mit vielen verschiedenen Trainingsdaten durchführt. Die visuelle Annahme, dass die SVM-Klassifikation genauer erscheint hat sich in der Validierung nicht gezeigt.  


\newpage
# Literatur



























