---
title: "Vergleich von Random Forest und Support Vector Machine"
author: "Sören Michaels"
date: "6.9.2020"
output: 
  html_document: 
    toc: yes
    keep_md: yes
  pdf_document:
    toc: true
bibliography: bib.bib
---


\newpage
# Einleitung

Ob, zur Analyse von Waldbränden, Dürren, Ölkatastrophen oder zur Erstellung von Karten über Landbedeckung, um eine computergestützte Verarbeitung der Daten kommt man heute in den Geowissenschaften nicht mehr herum. Nach der Daten akquise kommt die weitere Verarbeitung der Daten. Support Vector Machine und Random Forest sind sehr beliebte Algorithmen zur Klassifikation.

In dieser Arbeit werden diese zwei beliebte Machine Learning Algorithmen mit einander verglichen. Es wird eine kurze Beschreibung der Funktionsweise geben, bei der mögliche theoretische schwächen und stärken der beiden Algorithmen diskutiert werden. Darauf folgt ein Praktischer Teil, bei dem beide Algorithmen auf den gleichen Datensatz angewendet werden. Die Ergebnisse werden validiert und ausgewertet. Zum Schluss gibt es eine Fehleranalyse der methodischen Durchführung und ein Fazit.
\newpage
# Funktionsweise Random Forest

Random Forest ist ein Alorithmus der viele verschiedene randomisierte Entscheidungsbäume erstellt. Jeder Entscheidungsbaum trifft seine Entscheidung. Da die Bäume auf unterschiedliche Ergebnisse kommen entscheidet die Mehrheit der Bäume, wie die Daten klassifziert werden. 

Jeder Baum wird an den Knoten mit zufällig ausgewählten Eigenschaften oder einer Kombination aus verschieden Eigenschaften gespalten.

Es wird jeder Baum nicht mit dem orginal Datensatz erstellt, sondern mit einem sogenannten Bootstrapped Datensatz. Der Prozess um einen Bootstrapped Datensatz zu erstellen heißt Bagging.

Bagging (kurz für Bootstrap aggregating) funktioniert, wie folgt: Für den Orginaldatensatz $D$ mit der größe $n$, werden $m$ (Anzahl der Entscheidungsbäume) Trainingsdatensätze  $D_i$ erstellt, jeder mit der größe $n'$. Um die Traningsdatensätze $D_i$ zu erstellen, wird jeweils eine Zufallstichprobe mit zurücklegen erstellt. 

Ziel des Baggings ist es Overfitting zu vermeiden, dass Modell soll also nicht zu angepasst auf die Orginaldaten sein.

Es handelt sich beim Random Forest um sogenanntes Ensamble learning, es werden mehre Alghorithmen mit einander benutzt, um eine bessere Perfomance zu ereichen. Es werden mehrere schwache Klassifikatoren in diesem Fall Entscheidungsbäume miteinander "verschmolzen", um einen gut Klassifikator zu erstellen, dies wird auch Boosting genannt. Ziel ist es aus den schwachen Klassifikatoren, die Fehler behaftet sind, in unserem Fall die Entscheidungsbäume, einen Klassifkator zu erstellen, der wenige Fehler macht. Im Random Forest ist die Mehrheitswahl der Entscheidungsbäume die verschmelzung der schwachen Klassifikatoren. 
\newpage
# Funktionsweise Support Vector Machine

Der Support Vector Machine Algorithmus könnte genauer sein, als beliebte andere beliebte Techniken[@foody].
Das besondere ist, dass SVM nur binär Klassen teilen kann.
Es gibt aber Methoden, um mit SVM auch eine mehrklassige Klassifikation durch zu führen.
Eine Support Vector Machine teilt bei der Klassifikation die Klassengrenzen so ein, dass eine möglichst große Margin entsteht. Der Bereich der frei von Objekten ist soll also möglichst groß sein. Am einfachsten ist dies, wenn die Objekte linear teilbar sind. Dabei werden die Klasen grenzen mit einer linearen Hyperebene gezogen(Hyperebene = mathematischer Begriff für Ebenen mit n-Dimensionen, also im eindimensionalen Raum ist eine Hypereben ein Punkt im zweidimensionalen Raum eine Gerade usw.)[@adam].
Da man nicht immer die Klasen linear trennen kann kommt der sogenannte Kernel-Trick zum Einsatz.
Unter Kernel-Trick versteht man verschiedene Funktionen, um die Daten zu Transformieren.
Er funktioniert in dem die Trainingsdaten in höhere Dimensionen transformiert werden bis diese lineartrennbar sind.
Da die Anzahl der möglichen Dimensionen unendlich ist kann immer eine möglichkeit gefunden werden die Klassengrenzen zuteilen. 
Um aber die Genauigkeit zu erhöhen, die Rechenleistung zu verringern und Overfitting zuvermeiden, kann mit einer Soft Margin gearbeitet werden. Es wird also erlaubt, dass einige Objekte innerhalb der Marigin sich befinden oder sogar falsch klassifiziert werden.
Dies führt dazu, das Ausreißer weniger gewichtet werden und auf der anderen Seite dazu, dass eine Hyperebene schneller gefunden werden kann.
Um mehr als zwei Klassen zu klassifizieren wird in der bekannten SVM Implementation libsvm, die auch in der e1071 Library in R benutzt wird, die one-versus-one Strategie benutzt. 
Es es treten jeweils zwei Klassen gegeneinander an, die Klasse die Gewinnt erhält einen Punkt. 
Sind alle Klassen gegeneinander angetreten gewinnt die mit den meisten Punkten.


# Schwächen und Stärken der Algorithmen
- Tuning einfacher bei Random Forest
- Mehr Optionen bei SVM Kerneltrick, Cost, Gamma...
- Svm braucht Methoden zur multiclass

# Random Forest und Support Vector Machine in der Praxis

## Bildinformationen: Mit welchen Daten arbeiten wir

## Aufteilen des orginal Bild
Als erstes wird das Orginalbild in zwei Bilder geteilt.
Mit dem einem der Bilder werden die Trainingsdaten erstellt und das andere Bild wird dazu benutzt die Klassifikation anzuwenden und später zu validieren.
Das Aufteilen wurde in Qgis durchgeführt.
Es wurde zwischen West und Ost in der Mitte geteilt.


## Erstellung der Trainingsdaten
### Auswahl der Klassen
Als erstes muss entschieden werden in welche Klassen später klassifiert werden soll.
Dazu wird zu erst das Bild angeschaut und notiert welche Klassen und Unterklassen es gibt. Danach wird ein Shapefile erstellt. 
In diesem Shapefile speichern wir Polygone für die jeweiligen Klassen ab.
Der Shapefile wird später zum klassifizieren benutzt.



\newpage
## Durchführung Random Forest in R
1. **Laden der Libraries**

```r
library(randomForest)
library(sp)
library(raster)
library(rgdal)
```
2. **Setzen des Arbeitsverzeichnisses**

```r
#setwd("~/Dokumente/Arbeit/RFvsSVM")
```

3. **Laden der hymap Daten**

```r
TrainingsImage <- stack("TrainingsImage.tif")

names(TrainingsImage)[1:111] <- c(1:111)
TestImage <- stack("TestImage.tif")
names(TestImage)[1:111] <- c(1:111)
```
4. **Trainingsdaten laden und gegebenenfalls Transformieren**

```r
trainingsdaten <- shapefile("TrainingsDaten_v2.shp")
trainingsdaten <- spTransform(trainingsdaten, crs(TrainingsImage))
```
5. **Extrahieren der Klassifizierten Pixel mit Hilfe des Trainingsdaten Shapefile**

```r
extracted_pixels <- extract(TrainingsImage,trainingsdaten, df=T)


extracted_pixels$class <- as.factor(trainingsdaten$class[match(extracted_pixels$ID, seq(nrow(trainingsdaten)))])

extracted_pixels$ID <- NULL
```
6. **Erstellen, speichern und anwenden des Random Forest**

```r
# rf <- randomForest(na.roughfix(extracted_pixels[, names(extracted_pixels) != "class"]),extracted_pixels$class)
rf <- randomForest(extracted_pixels[, names(extracted_pixels) != "class"],extracted_pixels$class)
save(rf, file = "rfmodel.RData")
```

```r
#TestImage[is.na(TestImage)] <- 0
resultrf <- predict(
                  
                  TestImage,
                  rf,
                  filename = "classification_rf.tif",
                  overwrite = TRUE
)
```

7. **Klassifikation darstellen**


```r
plot(resultrf, 
     axes = FALSE, 
     box = FALSE,
     # col = c("burlywood4", #Acker 1
     #         "darkgreen", #Baum 2
     #         "gray", # graues Dach 4
     #         "green", # Kunstrasen 5
     #         "gray", # Lagerhallen 6
     #         "green", # Rasen 7
     #         "gray", # rotes Dach 8
     #         "darkgray", #Versiegelung 10
     #         "blue" # Wasser 11
     # )
)
```

![](Forschungsarbeit_files/figure-html/unnamed-chunk-8-1.png)<!-- -->


## Durchführung von Support Vector Machine in R

Die SVM Implementation befindet sich in der Library e1071[@fu].

1. **Library laden**

```r
library(e1071)
```
**Schritte 2-6 gegebenfalls wiederholen**


```r
svm <- svm(extracted_pixels[, names(extracted_pixels) != "class"],extracted_pixels$class)
```

```r
resultsvm <- predict(TestImage,
                  svm,
                  filename = "classification_svm.tif",
                  overwrite = TRUE
                  )

plot(resultsvm, 
     axes = FALSE, 
     box = FALSE
     # col = c("burlywood4", #Acker 1
     #         "darkgreen", #Baum 2
     #         "gray", # graues Dach 4
     #         "green", # Kunstrasen 5
     #         "gray", # Lagerhallen 6
     #         "green", # Rasen 7
     #         "gray", # rotes Dach 8
     #         "darkgray", #Versiegelung 10
     #         "blue" # Wasser 11
     # )
)
```

![](Forschungsarbeit_files/figure-html/unnamed-chunk-11-1.png)<!-- -->


## Validierung
\newpage
# Literatur



























